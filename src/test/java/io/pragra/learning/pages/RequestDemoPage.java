package io.pragra.learning.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RequestDemoPage {

    WebDriver driver;

    @FindBy(xpath = "//div[@class='page-header']/h2[1]")
    private WebElement pageHeading;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "company")
    private WebElement company;

    @FindBy(id = "first_name")
    private WebElement firstName;

    @FindBy(id = "last_name")
    private WebElement lastName;

    @FindBy(id = "00Nd0000007MFAl")
    private WebElement empCount;


    @FindBy(id="btnSubmit")
    private WebElement submit;


    public RequestDemoPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getHeading(){
        return this.pageHeading.getText();
    }

    public RequestDemoPage keyInEmail(String email){
        this.email.sendKeys(email);
        return this;
    }

    public RequestDemoPage keyInFirstName(String fname){
        this.firstName.sendKeys(fname);
        return this;
    }
    public RequestDemoPage keyInLastNamr(String lname){
        this.lastName.sendKeys(lname);
        return this;
    }

    public void submit(){
        this.submit.click();
    }
}

